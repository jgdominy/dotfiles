# sirlark's Dotfiles

## Installation

* git clone this package
* cd into the cloned directory
* run `setup.sh`

# What it does

* Common (All OS's)
  * zsh with oh-my-zsh and a powerline
  * vim with several plugins
    * c.vim
    * csv.vim
    * dockerfile.vim
    * html5.vim
    * jellybeans.vim
    * nerdtree
    * php.vim
    * python-mode
    * supertab
    * tabular
    * vim-airline
    * vim-fugitive
    * vim-go
    * vim-javascript
    * vim-json
    * vim-markdown
    * vim-refactor
    * vim-surround
  * some basic git config
* OSX specific
  * installs brew and several programs
    * midnight-commander
    * alfred
    * iterm2 with powerline fonts and a sane key map dynamic profile
    * several quicklook extensions for finder
      * qlmarkdown
      * quicklook-csv
      * webpquicklook
      * qlprettypatch
      * quicklook-json
      * qlcolorcode
      * qlstephen
    * docker
    * windows formatted drive write capability via osxfuse and ntfs-3g
  * Single sign on SSH keys

