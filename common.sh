function info() {
	echo -en "\033[34;1m"
	echo -n $@
	echo -e "\033[0m"
}

function alert() {
	echo -en "\033[33;1m"
	echo -n $@
	echo -e "\033[0m"
}

function error() {
	echo -en "\033[31;1m"
	echo -n $@
	echo -e "\033[0m"
}
