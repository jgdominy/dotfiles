#!/bin/bash

set -e

function brew_install() {
	#$1: brew command
	#$2: 'cask' if a cask install
	echo -n "Installing $1 ... "
	if brew $2 install $1 &> /tmp/$$.errmsg; then
		echo "Done"
	else
		if grep -q "is already installed" /tmp/$$.errmsg; then
			echo "Already installed, leaving it alone"
		else
			error "Failed!"
		fi
	fi
	rm -rf /tmp/$$.errmsg
}

source $srcdir/common.sh

info "We're going to need your password for some stuff here... trust us though, we're cool"
echo -n "Password: "
read -s UPW
echo
export UPW="$UPW"
if ! echo $UPW | sudo -S ls &> /dev/null; then
	error "That password doesn't seem to work... sorry"
	exit 2
fi

# Set up SSH keys and github
if cat ${HOME}/.ssh/id_ecdsa.pub &> /dev/null | grep -q "@wpengine.com"; then
	info "You've got an ecdsa SSH key set up"
	if ! security find-generic-password -s SSH &> /dev/null; then 
		echo "It doesn't look like the password for this key is in your keychain, I'll add it"
		alert "(FYI: I'm adding your provided password as the password for this key)"
		security add-generic-password -a $USER -s SSH -w "$UPW"
	fi
	export key_file=${HOME}/.ssh/id_ecdsa
else
	if cat ${HOME}/.ssh/id_rsa.pub &> /dev/null | grep -q "@wpengine.com"; then
		echo "You appear to have set up an rsa SSH key, you might want to look into upgrading your key"
		echo "format to ecdsa, but we'll use this for now."
		export key_file=${HOME}/.ssh/id_rsa
	else
		echo "It looks you don't have an SSH key set up, let's do that now"
		echo "We're going to set you up with a password protected ecdsa key"
		echo "The password is the same as your OSX login password, but is stored separately in your keychain."
		echo "You won't need to change the key password if/when you change your login password"
		expect <<EOF
eval spawn ssh-keygen -t ecdsa -f ${HOME}/.ssh/id_ecdsa -C "${USER}@wpengine.com";
expect "Enter passphrase (empty for no passphrase):";
send "$UPW\r";
expect "Enter same passphrase again:";
send "$UPW\r";
expect
EOF
		echo
		export key_file=${HOME}/.ssh/id_ecdsa
		info "Your zsh config will automatically add this key to your keychain (so you can basically forget"
		info "about it from now on), but you will be prompted to enter your password now."
		info "Choose 'allow always' to allow 'security' to access this specific password in your keychain"
		info "silently in future"
		security add-generic-password -a $USER -s SSH -w "$UPW"
		security find-generic-password -s SSH -w &> /dev/null
	fi
fi
echo "----- snip -----"
cat ${key_file}.pub
echo "----- snip -----"
alert "You want to copy the public key between the '-- snip --' lines above and put it into github."

echo "Turning on the locate database, because who wouldn't want that."
echo $UPW | sudo -S /bin/launchctl load -w /System/Library/LaunchDaemons/com.apple.locate.plist

# Install brew
if which brew &> /dev/null; then
	echo "Brew exists, nothing to see here, moving along..."
else
	echo "Installing brew"
	/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

brew tap caskroom/cask
if [ ! -d /Applications/iTerm.app ]; then
	brew_install iterm2 cask
fi

for p in qlmarkdown quicklook-csv webpquicklook qlprettypatch quicklook-json qlcolorcode qlstephen; do
	brew_install $p cask
done

echo -n "Installing osxfuse ... "
if expect -c 'eval spawn "brew cask install osxfuse"; expect {"Password:" {send "$env(UPW)\r"}; "is already installed" {send_user "Already installed, moving on"};} expect;'; then
	echo "Done"
else
	error "Failed"
fi

echo -n "Installing docker ... "
if expect -c 'eval spawn "brew cask install docker"; expect {"Password:" {send "$env(UPW)\r"}; "is already installed" {send_user "Already installed, moving on"};} expect;'; then
	echo "Done"
else
	error "Failed"
fi

brew tap homebrew/cask-fonts
for p in font-hack-nerd-font font-sourcecodepro-nerd-font font-terminus-nerd-font; do
	brew_install $p cask
done

brew tap sambadevi/powerlevel9k
brew_install powerlevel9k
mkdir -p ${HOME}/.oh-my-zsh/custom/themes
if [ ! -h ${HOME}/.oh-my-zsh/custom/themes/powerlevel9k ]; then
	ls -sf /usr/local/opt/powerlevel9k ${HOME}/.oh-my-zsh/custom/themes/powerlevel9k
fi

for p in midnight-commander ntfs-3g docker-compose docker-completion zsh; do
	brew_install $p
done

read -p "Do you want Alfred (a better spotlight)? [y/n] " yn
if [[ "$yn" == "y" ]]; then 
	brew cask install alfred
	alert "To finish setting Alfred up, you need to assign a keyboard shortcut to it. command-space is generally good choice"
	alert " 1. Remove (or replace) the command-space shortcut key for spotlight"
	alert " 2. Setup command-space as the shortcut to open alfred"
	read
else 
	echo "Cool, skipping it!"
fi

read -p "Do you want Firefox in addition to Chrome? [y/n] " yn
if [[ "$yn" == "y" ]]; then brew cask install firefox; else echo "Cool, skipping it!"; fi
read -p "Do you want android tools (adb etc)? [y/n] " yn
if [[ "$yn" == "y" ]]; then brew cask install android-platform-tools; else echo "Cool, skipping it!"; fi

info "Setting up OSX specific dotfiles"
alert "This will overwrite files that exists, but backups will be placed in /tmp/dotfile-backups"
rsync -arbv --backup-dir=/tmp/dotfile-backups "$srcdir/skel/osx/" "$destdir"
alert "We've installed a keyboard profile ('Terminal Sanity') for iterm2. You should open the iterm2 preferences and set it as your default profile"

