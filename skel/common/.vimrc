" Fix Delete (Fn-Backspace)
set <Del>=

" Make F2 toggle paste mode
nmap <F2> :set paste!<CR>

" Syntax highlighting
syntax on

" Incremetal highlighted search
set incsearch hlsearch

" Split windows in an intuitive way
set splitright splitbelow

" Colorscheme
colorscheme jellybeans

" Fix NERDTrees ^G displaying on every node line
let g:NERDTreeNodeDelimiter = "\u00a0"
