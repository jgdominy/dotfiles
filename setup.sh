#!/bin/bash

set -e

export srcdir=`dirname $0`
if [[ -n "$1" ]]; then
	export destdir="$1"
else
	export destdir="$HOME"
fi

source $srcdir/common.sh

info "Setting up in $destdir"

if [ ! which zsh &> /dev/null ]; then 
	if [[ -x /usr/bin/curl ]]; then
		sh -c "$(/usr/bin/curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
	elif [[ -x /user/bin/wget ]]; then
		sh -c "$(/usr/bin/wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
	else
		error "Neither curl nor wget are available to install oh-my-zsh :("
		exit 3
	fi
fi

if uname | grep -q 'Darwin'; then
	echo "Running on Mac OSX; doing OSX specific stuff"
	./setup-osx.sh
else
	echo "Assuming Linux; doing Linux specific stuff"
	./setup-linux.sh
fi

info "Setting up common dotfiles"
alert "This will overwrite files that exists, but backups will be placed in /tmp/dotfile-backups"
rsync -arbv --backup-dir=/tmp/dotfile-backups "$srcdir/skel/common/" "$destdir"

function safe_clone() {
	if [ -d `basename $1` ]; then
		info "$p already installed"
	else
		git clone https://github.com/$1
	fi
}

mkdir -p ${destdir}/.vim/pack/plugins/start
cd ${destdir}/.vim/pack/plugins/start
for p in vim-scripts/c.vim chrisbra/csv.vim ekalinin/dockerfile.vim othree/html5.vim nanotech/jellybeans.vim scrooloose/nerdtree stanangeloff/php.vim ervandew/supertab godlygeek/tabular vim-airline/vim-airline tpope/vim-fugitive fatih/vim-go pangloss/vim-javascript elzr/vim-json gabrielelana/vim-markdown luchermitte/vim-refactor tpope/vim-surround python-mode/python-mode; do
	safe_clone $p
done
cd python-mode
git submodule update --init --recursive
cd ..

info "Set up your git identity ..."
read -p "Work email address: " email
git config --global user.email $email
read -p "Full name: " name
git config --global user.name $name

